import robocode.Robot;
import robocode.ScannedRobotEvent;

public class Giro extends Robot {

	@Override
	public void run() {
		
		while(true) {
			turnLeft(45);
		}
	}
	
	@Override
	public void onScannedRobot(ScannedRobotEvent event) {
		if(event.getDistance()<200) {
			fire(3);
		}else {
			fire(1);
		}
	}
	
}
