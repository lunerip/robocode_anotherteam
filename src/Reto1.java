import robocode.BulletHitEvent;
import robocode.BulletMissedEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;

public class Reto1 extends Robot {
	boolean rastreando, disparando;
	double power;
	
	public void run() {
		rastreando = true;
		disparando = false;
		power = 1;
		while(true) {
			if(rastreando) {
				turnLeft(10);
			}else if(disparando) {
				fire(power);
			}
		}
	}
	
	@Override
	public void onScannedRobot(ScannedRobotEvent event) {
		rastreando = false;
		disparando = true;
	}
	

	public void onBulletHit(BulletHitEvent event){
		if(power<3) {
			power++;
		}
	}
	
	 public void onBulletMissed(BulletMissedEvent event) {
		power = 1;
		rastreando = true;
		disparando = false;
	}
	
	
}
