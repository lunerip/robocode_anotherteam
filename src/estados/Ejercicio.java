package estados;
import robocode.*;

public class Ejercicio extends Robot {
	private Estado estado;
	private long tiempoH;
	private double potencia;
	private String nombre;
	private String nombre2;
	
	@Override
	public void run() {
		nombre = "";
		nombre2 = "1";
		potencia = 0.1;
		estado = Estado.BUSCANDO;
		
		while(true) {
			//SEBAS
			switch(estado) {
			case BUSCANDO:
				//SEBAS
			case DISPARANDO:
				
				fire(potencia);
				break;
				//NERI
			case DEFENDIENDO:
				//LUIS
				break;
			case CHOQUE:
				//LUIS
			default:
				break;
			}
		}
	}
	@Override
	public void onScannedRobot(ScannedRobotEvent event) {
		stop();
		estado = Estado.DISPARANDO;
		if(event.getDistance() < 50) {
			potencia = 4;
		}
	}
	
	public void onBulletMissed(BulletMissedEvent evnt){
		potencia = 0.1;
		estado = Estado.BUSCANDO;
	}
	public void onBulletHit(BulletHitEvent evnt){
		if (potencia < 3) {
			potencia += 1;
		}
		nombre2 = evnt.getName();
	}
	public void onHitByBullet(HitByBulletEvent event){
		if(estado != Estado.DISPARANDO || nombre != nombre2) {
			estado = Estado.DEFENDIENDO;
			tiempoH = System.currentTimeMillis() ;
			if(System.currentTimeMillis() - tiempoH >= 50) {
				estado = Estado.BUSCANDO;
			}
		}
		if(nombre == nombre2) {
			potencia = 4;
		}
		nombre = event.getName();
		System.out.println(nombre);
	}
	public void onHitRobot(HitRobotEvent event){
		estado = Estado.CHOQUE;
		turnGunLeft(event.getBearing());
		fire(3);
	}
	enum Estado{
		BUSCANDO, 
		DISPARANDO,
		DEFENDIENDO,
		CHOQUE
	}
}