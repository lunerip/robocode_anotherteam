package estados;

import robocode.BulletHitEvent;
import robocode.BulletMissedEvent;
import robocode.HitByBulletEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;

public class RobotEstado extends Robot {
	
	private Estado estado;
	private long tiempoHuida;
	public double power;
	
	@Override
	public void run() {
		estado = Estado.GIRANDO;
		while(true) {
			switch(estado) {
			case GIRANDO:
				turnRight(5);
				break;
			case DISPARANDO:
				fire(power);
				break;
			case HUYENDO:
				ahead(10);
				if(System.currentTimeMillis() - tiempoHuida >=3000) {
					estado = Estado.GIRANDO;
				}
				break;
			
			default:
				break;
			}
		}
	}
	
	@Override
	public void onScannedRobot(ScannedRobotEvent event) {
		estado = Estado.DISPARANDO;
	}
	
	@Override
	public void onBulletMissed(BulletMissedEvent event) {
		power = 1;
		estado = Estado.GIRANDO;
	}
	
	@Override
	public void onHitByBullet(HitByBulletEvent event) {
		if(estado == Estado.GIRANDO) {
			estado = Estado.HUYENDO;
			tiempoHuida = System.currentTimeMillis();
		}
	}
	
	@Override
	public void onBulletHit(BulletHitEvent event) {
		if(power<3) {
			power++;
		}
	}
	
	enum Estado{
		GIRANDO,
		DISPARANDO,
		HUYENDO
	}
	
}
