package estados;

import robocode.HitRobotEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;

public class Tracker extends Robot {
	double ubicacion, distancia, power;
	Estado estado;
	@Override
	public void run() {
		while(true){
			turnLeft(5);
		}
	}
	
	@Override
	public void onScannedRobot(ScannedRobotEvent event) {
		ubicacion = event.getBearing();
		distancia = event.getDistance();
		ahead(distancia + 10);
		//fire(3);
	}
	
	public void onHitRobot(HitRobotEvent event) {
		//fire(3);
		estado = Estado.CHOQUE;
	}
	
	enum Estado{
		PATRULLAJE,
		ONEvsONE,
		REGRESAR,
		EVADIR,
		ALERTA,
		CHOQUE,
		APRENDIDO,
		FALLO
	}
}
