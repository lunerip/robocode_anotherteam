package estados;

import robocode.*;

public class OnevsOne extends Robot {
	
	Estado estado; 
	double distancia;
	boolean detectado;
	double power;
	
	@Override
	public void run() {
		estado = Estado.ONEvsONE;
		detectado = false;
		power = 1;
		while(true) {
			if(estado == Estado.ONEvsONE) {
				if(detectado) {
					ahead(100);
				}else {
					turnLeft(5);
				}
			}
		}
	}
	
	@Override
	public void onScannedRobot(ScannedRobotEvent event) {
		distancia = event.getDistance();
		if(!detectado) {
			ahead(distancia * 1.1);
			detectado = true;
			fire(power);
		}
		
	
	}

	@Override
	public void onBulletMissed(BulletMissedEvent event) {
		power = 1;
		detectado = false;
	}
	
	@Override
	public void onBulletHit(BulletHitEvent event) {
		if(power<3) {
			power++;
		}
	}
	
	enum Estado{
		ONEvsONE;
	}
}
