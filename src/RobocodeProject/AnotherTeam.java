package RobocodeProject;
//Luis Enrique Neri Perez A01745995
//Fernando Sebastian Silva Miramontes A01746925
//Luis Miguel Baqueiro Vallejo A01745997

import java.awt.Color;
import robocode.*;

public class AnotherTeam extends AdvancedRobot {
	/*
	:::     ::::    :::  ::::::::  ::::::::::: :::    ::: :::::::::: :::::::::         ::::::::::: ::::::::::     :::     ::::    ::::  
  :+: :+:   :+:+:   :+: :+:    :+:     :+:     :+:    :+: :+:        :+:    :+:            :+:     :+:          :+: :+:   +:+:+: :+:+:+ 
 +:+   +:+  :+:+:+  +:+ +:+    +:+     +:+     +:+    +:+ +:+        +:+    +:+            +:+     +:+         +:+   +:+  +:+ +:+:+ +:+ 
+#++:++#++: +#+ +:+ +#+ +#+    +:+     +#+     +#++:++#++ +#++:++#   +#++:++#:             +#+     +#++:++#   +#++:++#++: +#+  +:+  +#+ 
+#+     +#+ +#+  +#+#+# +#+    +#+     +#+     +#+    +#+ +#+        +#+    +#+            +#+     +#+        +#+     +#+ +#+       +#+ 
#+#     #+# #+#   #+#+# #+#    #+#     #+#     #+#    #+# #+#        #+#    #+#            #+#     #+#        #+#     #+# #+#       #+# 
###     ### ###    ####  ########      ###     ###    ### ########## ###    ###            ###     ########## ###     ### ###       ### 
	
	Para el robot Final, empezamos a haciendo una abstracción de lo que queremos hacer.

	GANAR: obviamente queríamos ganar.
	BUSCAR PATRONES: esto es algo que hemos estado pensando, que puede ser factible para robots no muy bien programados, 
	que es establecer el patrón de huida de un robot, que hace cuando la bala le pega.
	DPS: es una forma de decir que vamos a buscar hacer la mayor cantidad de daño, no vamos a buscar atacar a muerte a un jugador.
	ONEvsONE: queremos establecer una estrategia completamente diferente cuando solo tenemos un contrincante.
	
	Las prioridades de nuestro robot son:
		·Daño
		·Matar
		·Precisión
		·Supervivencia
		
	Las preocupaciones de nuestro robot son: estar en el centro del mapa, y que uno de nuestros enemigos este en nuestros lados.
	Luego de esto, definimos los estados que queríamos que nuestro robot tuviera, con esto, y analizándonos, nos dimos cuenta que 
	había estados que no eran estados, si no eran eventos que pasaban dentro de los estados,
	
	Y al final definimos los métodos que cada estado iba a usar, nos dimos cuenta que algunos estados iban a usar un método para 
	perseguir, así que decidimos poner eso como método que íbamos a hacer, o una serie de acciones que sucederían cada que lo llamaras.
		
	*/
	
	// Variables
	private Estado estado;
	private double alto;
	private double ancho;
	private double distanciaTecho;
	private double distanciaIzquierda;
	private double distanciaAlPuntoMasCercano;
	private double angulo;
	private boolean sentidoHorario = true;
	private int Direccion1vs1=1;
	static int derrota = 0;
	private double diferenciaAngulosChoque;
	private double posibleEstado;
	private double posibleSiguientePosicion;
	private String enemigo = "";
	private int choque;
	private long Tiempo = 0;
	
	public void run() {
		estado = Estado.REGRESAR; // El robot inicia en estado REGRESAR
		
		// Si hemos perdido más de 4 veces o nos queda menos de 25 de energía o queda un solo enemigo entramos al estado ONEvsONE
		if(derrota > 4 || getEnergy() < 25 || getOthers() == 1 || getOthers() > 6){
			estado = Estado.ONEvsONE;
		}
		while(true) {
			if(Tiempo < System.currentTimeMillis() - 500) {
				choque = 0;
			}
			//Calcula cuando hay que cambiar a 1vs1
			if(derrota > 4 || getEnergy() < 25 || getOthers() == 1) {
				estado = Estado.ONEvsONE;
			}
			switch(estado) {	
			/*
				'########::'########::'######:::'########::'########::'######:::::'###::::'########::
				##.... ##: ##.....::'##... ##:: ##.... ##: ##.....::'##... ##:::'## ##::: ##.... ##:
				##:::: ##: ##::::::: ##:::..::: ##:::: ##: ##::::::: ##:::..:::'##:. ##:: ##:::: ##:
				########:: ######::: ##::'####: ########:: ######:::. ######::'##:::. ##: ########::
				##.. ##::: ##...:::: ##::: ##:: ##.. ##::: ##...:::::..... ##: #########: ##.. ##:::
				##::. ##:: ##::::::: ##::: ##:: ##::. ##:: ##:::::::'##::: ##: ##.... ##: ##::. ##::
				##:::. ##: ########:. ######::: ##:::. ##: ########:. ######:: ##:::: ##: ##:::. ##:
				..:::::..::........:::......::::..:::::..::........:::......:::..:::::..::..:::::..::
			 */
			case REGRESAR:
				// Encargado de regresar a modo patrullar si es que ya terminó de buscar a un enemigo tras choque
				setAllColors(Color.GRAY);
				sentidoHorario = !sentidoHorario;
				angulo = getHeading();
				alto = getY();
				ancho = getX();
				distanciaTecho = getBattleFieldHeight() - alto;
				distanciaIzquierda = getBattleFieldWidth() - ancho;
				setMaxVelocity(200);
				
				//Calcula cual es el punto mas cerca del cuadrado
				if(alto < distanciaTecho) { 
					if(alto  < ancho) { 
						if(alto < distanciaIzquierda) {
							//Mas cerca del suelo
							distanciaAlPuntoMasCercano = alto - getHeight();
							angulo = angulo - 180;
						}
						else {
							//Mas cerca de la derecha
							distanciaAlPuntoMasCercano = distanciaIzquierda - getHeight();
							angulo = angulo - 90;
						}
					}else {
						if(ancho < distanciaIzquierda) {
							//Mas cerca de la izquierda
							distanciaAlPuntoMasCercano = ancho - getHeight();
							angulo = angulo - 270;
						}
						else {
							//Mas cerca de la derecha
							distanciaAlPuntoMasCercano = distanciaIzquierda - getHeight();
							angulo = angulo - 90;
						}
					}
				}
				else {
					if(distanciaTecho  < ancho) {
						if(distanciaTecho < distanciaIzquierda) {
							//Mas cerca del techo
							distanciaAlPuntoMasCercano = distanciaTecho - getHeight();
							
						}
						else {
							//Mas cerca de la derecha
							distanciaAlPuntoMasCercano = distanciaIzquierda - getHeight();
							angulo = angulo - 90;
						}
					}
					else {
						if(ancho < distanciaIzquierda) {
							//Mas cerca de la izquierda
							distanciaAlPuntoMasCercano = ancho - getHeight();
							angulo = angulo - 270;
						}
						else {
							//Mas cerca de la derecha
							distanciaAlPuntoMasCercano = distanciaIzquierda - getHeight();
							angulo = angulo - 90;
						}
					}
				}
				turnLeft(angulo);
				ahead(distanciaAlPuntoMasCercano);
				//Voltea el robot para empezar el patrullaje
				if(sentidoHorario) {
					turnRight(90);
				}else {
					turnLeft(90);
				}
				estado = Estado.PATRULLAJE;
				break;
			
				/*
				'########:::::'###::::'########:'########::'##::::'##:'##:::::::'##::::::::::'###::::::::::'##:'########:
				##.... ##:::'## ##:::... ##..:: ##.... ##: ##:::: ##: ##::::::: ##:::::::::'## ##::::::::: ##: ##.....::
				##:::: ##::'##:. ##::::: ##:::: ##:::: ##: ##:::: ##: ##::::::: ##::::::::'##:. ##:::::::: ##: ##:::::::
				########::'##:::. ##:::: ##:::: ########:: ##:::: ##: ##::::::: ##:::::::'##:::. ##::::::: ##: ######:::
				##.....::: #########:::: ##:::: ##.. ##::: ##:::: ##: ##::::::: ##::::::: #########:'##::: ##: ##...::::
				##:::::::: ##.... ##:::: ##:::: ##::. ##:: ##:::: ##: ##::::::: ##::::::: ##.... ##: ##::: ##: ##:::::::
				##:::::::: ##:::: ##:::: ##:::: ##:::. ##:. #######:: ########: ########: ##:::: ##:. ######:: ########:
				..:::::::::..:::::..:::::..:::::..:::::..:::.......:::........::........::..:::::..:::......:::........::
				 */
				
			case PATRULLAJE: 
				//Se mueve por el circuito cerca de los bordes
				setAllColors(new Color(226, 3, 193)); // ROSA
				setAdjustRadarForRobotTurn(true);
				setAdjustGunForRobotTurn(true);
				turnRadarRightRadians(360); // Busca en todos los lados
				break; 
			/*
				'#######::'##::: ##:'########::::'##::::'##::'######::::::'#######::'##::: ##:'########:
				##.... ##: ###:: ##: ##.....::::: ##:::: ##:'##... ##::::'##.... ##: ###:: ##: ##.....::
				##:::: ##: ####: ##: ##:::::::::: ##:::: ##: ##:::..::::: ##:::: ##: ####: ##: ##:::::::
				##:::: ##: ## ## ##: ######:::::: ##:::: ##:. ######::::: ##:::: ##: ## ## ##: ######:::
				##:::: ##: ##. ####: ##...:::::::. ##:: ##:::..... ##:::: ##:::: ##: ##. ####: ##...::::
				##:::: ##: ##:. ###: ##:::::::::::. ## ##:::'##::: ##:::: ##:::: ##: ##:. ###: ##:::::::
				 #######:: ##::. ##: ########::::::. ###::::. ######:::::. #######:: ##::. ##: ########:
				:.......:::..::::..::........::::::::...::::::......:::::::.......:::..::::..::........::
			 */
				
			case ONEvsONE: 
				//Es el modo más violento del robot, rastrea y ataca con mucha potencia además moverse para esquivar las balas
				setAdjustRadarForRobotTurn(true);
				setAdjustGunForRobotTurn(true);
				turnRadarRightRadians(360); //Busca Enemigos
				break;
				/*
				:######::'##::::'##::'#######:::'#######::'##::::'##:'########:
				##... ##: ##:::: ##:'##.... ##:'##.... ##: ##:::: ##: ##.....::
				##:::..:: ##:::: ##: ##:::: ##: ##:::: ##: ##:::: ##: ##:::::::
				##::::::: #########: ##:::: ##: ##:::: ##: ##:::: ##: ######:::
				##::::::: ##.... ##: ##:::: ##: ##:'## ##: ##:::: ##: ##...::::
				##::: ##: ##:::: ##: ##:::: ##: ##:.. ##:: ##:::: ##: ##:::::::
				. ######:: ##:::: ##:. #######::. ##### ##:. #######:: ########:
				:......:::..:::::..:::.......::::.....:..:::.......:::........::
				 */
			case CHOQUE: 
				//Cuando alguien te choca rastrea al agresor y lo destruye
				setAllColors(new Color(255,215,0)); // DORADO
				setTurnRadarLeft(999); //Busca Enemigos
				break;
				/*
				########:::::'###::::'########::'########:'##:::'##:
				##.... ##:::'## ##::: ##.... ##:... ##..::. ##:'##::
				##:::: ##::'##:. ##:: ##:::: ##:::: ##:::::. ####:::
				########::'##:::. ##: ########::::: ##::::::. ##::::
				##.....::: #########: ##.. ##:::::: ##::::::: ##::::
				##:::::::: ##.... ##: ##::. ##::::: ##::::::: ##::::
				##:::::::: ##:::: ##: ##:::. ##:::: ##::::::: ##::::
				..:::::::::..:::::..::..:::::..:::::..::::::::..:::::
				 */
			case PARTY: //Festejo
				stop();
				//COLOR RANDOM
				setColors(new Color(((int) (Math.random() * 256) + 1),(int) (Math.random() * 256) + 1,(int) (Math.random() * 256) + 1),new Color(((int) (Math.random() * 256) + 1),(int) (Math.random() * 256) + 1,(int) (Math.random() * 256) + 1),new Color(((int) (Math.random() * 256) + 1),(int) (Math.random() * 256) + 1,(int) (Math.random() * 256) + 1),new Color(((int) (Math.random() * 256) + 1),(int) (Math.random() * 256) + 1,(int) (Math.random() * 256) + 1),new Color(((int) (Math.random() * 256) + 1),(int) (Math.random() * 256) + 1,(int) (Math.random() * 256) + 1));
				setTurnRight(999999);
				break;
				
			default:
				break;
			}
		}
	}

	
/*
 #######                                                
 #        #    #  ######  #    #  #####   ####    ####  
 #        #    #  #       ##   #    #    #    #  #      
 #####    #    #  #####   # #  #    #    #    #   ####  
 #        #    #  #       #  # #    #    #    #       # 
 #         #  #   #       #   ##    #    #    #  #    # 
 #######    ##    ######  #    #    #     ####    #### 
*/
	public void onScannedRobot(ScannedRobotEvent event) {
		
		if(estado == Estado.CHOQUE) {
			// detecta al enemigo que nos choco y lo busca y va a atacarlo
			if(enemigo.equals(event.getName()) && event.getDistance()<150) { //Si esta cerca el enemigo que localizamos que nos choco
				diferenciaAngulosChoque=event.getBearingRadians()+getHeadingRadians();
		        posibleEstado=event.getVelocity() * Math.sin(event.getHeadingRadians() -diferenciaAngulosChoque);
		        setTurnRadarLeftRadians(getRadarTurnRemainingRadians());
		        posibleSiguientePosicion = robocode.util.Utils.normalRelativeAngle(diferenciaAngulosChoque- getGunHeadingRadians()+posibleEstado/22);
	            setTurnGunRightRadians(posibleSiguientePosicion);
	            setTurnRightRadians(robocode.util.Utils.normalRelativeAngle(diferenciaAngulosChoque-getHeadingRadians()+posibleEstado/getVelocity()));
	            setAhead((event.getDistance())*Direccion1vs1);
	            setFire(3);
			}else if(enemigo.equals(event.getName()) && event.getDistance()>150) { //Cuando el enemigo se aleja
				stop();
				estado = Estado.REGRESAR;
			}
		}
		
		if(estado == Estado.ONEvsONE) {
			setColors(new Color(((int) (Math.random() * 256) + 1),(int) (Math.random() * 256) + 1,(int) (Math.random() * 256) + 1),new Color(((int) (Math.random() * 256) + 1),(int) (Math.random() * 256) + 1,(int) (Math.random() * 256) + 1),new Color(((int) (Math.random() * 256) + 1),(int) (Math.random() * 256) + 1,(int) (Math.random() * 256) + 1),new Color(((int) (Math.random() * 256) + 1),(int) (Math.random() * 256) + 1,(int) (Math.random() * 256) + 1),new Color(((int) (Math.random() * 256) + 1),(int) (Math.random() * 256) + 1,(int) (Math.random() * 256) + 1)); // Color aleatorio ( para despistar a las personas)
			//Detecta al enemigo que queda y lo fija en su radar
			diferenciaAngulosChoque=event.getBearingRadians()+getHeadingRadians();
	        posibleEstado=event.getVelocity() * Math.sin(event.getHeadingRadians() -diferenciaAngulosChoque);
	        setTurnRadarLeftRadians(getRadarTurnRemainingRadians());
	        if(Math.random()>.9){
	            setMaxVelocity((12*Math.random())+12);
	        }
	        //Calcula la distancia para saber la potencia y cuanto se necesita acercar
	        if (event.getDistance() > 150) {
	            posibleSiguientePosicion = robocode.util.Utils.normalRelativeAngle(diferenciaAngulosChoque- getGunHeadingRadians()+posibleEstado/22);
	            setTurnGunRightRadians(posibleSiguientePosicion);
	            setTurnRightRadians(robocode.util.Utils.normalRelativeAngle(diferenciaAngulosChoque-getHeadingRadians()+posibleEstado/getVelocity()));
	            setAhead((event.getDistance() - 140)*Direccion1vs1);
	            setFire(1);
	        }
	        else{
	            posibleSiguientePosicion = robocode.util.Utils.normalRelativeAngle(diferenciaAngulosChoque- getGunHeadingRadians()+posibleEstado/15);
	            setTurnGunRightRadians(posibleSiguientePosicion);
	            setTurnLeft(-90-event.getBearing());
	            setAhead(100*Direccion1vs1);
	            setFire(3);
	        }
		}
		if(estado == Estado.PATRULLAJE){
			//Busca enemigos
			diferenciaAngulosChoque=event.getBearingRadians()+getHeadingRadians();
	        posibleEstado=event.getVelocity() * Math.sin(event.getHeadingRadians() -diferenciaAngulosChoque);
	        setAhead(900);
	        setMaxVelocity(200);
	        //Si esta lejos sigue buscando y si tiene mucha energia les dispara
	        if (event.getDistance() > 250 && event.getVelocity() > 0) {
	        	posibleSiguientePosicion = robocode.util.Utils.normalRelativeAngle(diferenciaAngulosChoque- getGunHeadingRadians()+posibleEstado/22);
	        	setTurnGunRightRadians(posibleSiguientePosicion);
	        	if(getEnergy() > 50) {
	        		setFire(1);
	        	}
	        }
	        //si esta a una distancia considerable les dispara con 2 y lo sigue
	        if (event.getDistance() < 250 && event.getDistance() > 50) {
	        	setTurnRadarLeftRadians(getRadarTurnRemainingRadians());
	            posibleSiguientePosicion = robocode.util.Utils.normalRelativeAngle(diferenciaAngulosChoque- getGunHeadingRadians()+posibleEstado/22);
	            setTurnGunRightRadians(posibleSiguientePosicion);
	            setFire(2);
	        }
	        //le dispara con 3 si se acerca mas
	        if(event.getDistance() < 50){
	        	setTurnRadarLeftRadians(getRadarTurnRemainingRadians());
	            posibleSiguientePosicion = robocode.util.Utils.normalRelativeAngle(diferenciaAngulosChoque- getGunHeadingRadians()+posibleEstado/15);
	            setTurnGunRightRadians(posibleSiguientePosicion);
	            setFire(3);
	        }
		}
	}
	
	public void onHitRobot(HitRobotEvent event) {
		//Si nos pegan cambia a choque y detecta el nombre de quien nos choco para matarlo o perseguirlo
		if(estado != Estado.ONEvsONE ) {
			setAllColors(new Color(255,215,0));
			setAdjustRadarForRobotTurn(true);
			setAdjustGunForRobotTurn(true);
			enemigo = event.getName();
			estado = Estado.CHOQUE;
		}
	}		
	
	public void onRobotDeath(RobotDeathEvent event){
		//Detecta si solo queda uno para atacarlo solo a el
		if(getOthers() == 1) {
			stop();
			estado = Estado.ONEvsONE;
		}
		//Detecta si el enemigo que detectamos en choque murio para dejar de perseguirlo
		else if(event.getName().equals(enemigo) && estado == Estado.CHOQUE) {
			stop();
			estado = Estado.REGRESAR;
		}
	}
	
	public void onHitWall(HitWallEvent event){
		//Si estas en 1vs1 cambia de sentido
		if(estado == Estado.ONEvsONE || estado == Estado.CHOQUE) {
     Direccion1vs1=-Direccion1vs1;
     if(choque == 0) {
         Tiempo = System.currentTimeMillis();
     }
     choque++;
     if (choque > 2){
     	stop();
     	setTurnLeft(90);
     	setAhead(10);
     }
		}
		//Hace que "siga" la pared
     if(estado == Estado.PATRULLAJE) {
     	if(sentidoHorario) {
     		turnRight(90);
     	}
     	else {
     		turnLeft(90);
     	}
     }
 }
	public void onWin() {
		stop();
		estado = Estado.PARTY; 
	}
	public void onDeath(DeathEvent event) {
		derrota++;
	}
/*
 #######                                                
 #         ####   #####    ##    #####    ####    ####  
 #        #         #     #  #   #    #  #    #  #      
 #####     ####     #    #    #  #    #  #    #   ####  
 #             #    #    ######  #    #  #    #       # 
 #        #    #    #    #    #  #    #  #    #  #    # 
 #######   ####     #    #    #  #####    ####    ####
 */
	enum Estado{ 
		PATRULLAJE,
		ONEvsONE,
		REGRESAR,
		CHOQUE,
		PARTY
	}
}